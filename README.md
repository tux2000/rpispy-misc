# Example scripts for the Raspberry Pi

# Python Scripts
* gertduino
* gnuplot
    * gnuplot_example.png      - Example plot
    * gnuplot_generate_data.py - Generate some example data
    * mydata.dat               - Example data
* mcp3008
    * mcp3008_tmp36.py    - Read analogue sensors using an MCP3008 ADC
    * mcp3008_joystick.py - Read X,Y and Switch values of analogue joystick using an MCP3008 ADC
* mcp23017
    * mcp23017_inputs.py  - Configure and read inputs
    * mcp23017_outputs.py - Configure and read outputs
* minecraft
    * api_test.py         - Test API is working
    * castle.py           - Castle building script
    * counterstrike.py    - Operation Counterstrike game
* python
    * detect_network_example.py
    * stepper.py          - Simple stepper motor control
    * ultrasonic_1.py     - Ultrasonic sensor example
    * ultrasonic_2.py     - Ultrasonic sensor calibration
    * bh1750.py           - BH1750 light sensor module
    * bmp180.py           - BMP180 pressure & temperature module

# Tutorials and Guides
Visit my site for more information :
[http://www.raspberrypi-spy.co.uk/](http://www.raspberrypi-spy.co.uk/)